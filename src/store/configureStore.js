import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers/rootReducer';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';

export default function configureStore(initialState) {

    const immutableStateInvariant = reduxImmutableStateInvariant();

    const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(immutableStateInvariant)
    );

    return store;
}