import React, {PropTypes} from 'react';

class App extends React.Component {

	render() {

		return	(
				<div>
					<main>
						<div className="container">					
							{this.props.children}				
						</div>
					</main>
				</div>
				);
	}
}

App.propTypes = {
	children: PropTypes.object.isRequired
};

export default App;