import React, {PropTypes} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as feedActions from '../../actions/feedActions';
import * as constants from './../../constants/constants';
import MainPage from '../../components/main/MainPage';
import WebSocketService from './../../websockets/WebSocketService';

class MainContainer extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.wsService = new WebSocketService();
        this.dispatcher = this.dispatcher.bind(this);
    }

    dispatcher(data) {
        this.props.actions.receiveMessage(data);
    }

    componentWillMount() {
        this.wsService.connect(constants.url, this.dispatcher);
    }

    render() {

        return (
            <div>
                <MainPage feed={this.props.feed} />
            </div>
        );
    }
}

MainContainer.defaultProps = {
    feed: {
        nbTransactionTreated: 0,
        nbTransactionValid: 0,
        data: []
    }
};

MainContainer.propTypes = {
    feed: React.PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        feed: state.feed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(feedActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);
