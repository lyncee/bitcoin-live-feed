// Feed url
export const url = "wss://ws.blockchain.info/inv";
// Satoshi ratio
export const satoshiRatio = 0.00000001;
// Value in bitcoin to filter out any transactions with amount < threshold
export const threshold = 1000;
// Value in ms to ping the server to maintain the connection
export const  refreshDelay = 30000;
// Value in ms to reconnect when the connection is closed
export const  reconnectDelay = 5000;