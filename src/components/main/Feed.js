import React, {PropTypes} from 'react';
import TransactionList from './TransactionList';

const Feed = (props) => {

    return (
        <ul className="feed">
            {
                props.feed.map(function (item, index) {
                    return (
                        <li key={index} className={(index % 2 == 0) ? "tag even" : "tag odd"}>
                            <span className="time">{item.time}</span>
                            <span> : Total amount = </span>
                            <span className="total">{item.total}</span>
                            <TransactionList transactions={item.transactions} />
                        </li>);
                    })
            }
        </ul>
    );
};

Feed.propTypes = {
    feed: React.PropTypes.arrayOf(React.PropTypes.shape({
        total: React.PropTypes.number.isRequired,
        time: React.PropTypes.string.isRequired,
        transactions: React.PropTypes.array.isRequired
    })),
};

export default Feed;