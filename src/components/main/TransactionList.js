import React, {PropTypes} from 'react';

const TransactionList = (props) => {

    return (
            <div className="transactions">
                {
                    props.transactions.map(function (transaction, index) {
                        return  (
                            <div key={index}>
                                <span className="address">{transaction.address}</span>
                                <span> : </span>
                                <span className="amount">{transaction.amount}</span>
                            </div>
                        );
                    })
                }
            </div>
    );
};

TransactionList.propTypes = {
    transactions: React.PropTypes.arrayOf(React.PropTypes.shape({
        address: React.PropTypes.string.isRequired,
        amount: React.PropTypes.number.isRequired
    })),
};

export default TransactionList;