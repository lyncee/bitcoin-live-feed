import React, {PropTypes} from 'react';

const Statistics = (props) => {

    return (
        <div className="statistics">
            <span>Transactions (Valid/Treated): </span>
            <span className="total">{props.valid}</span>
            <span> / </span>
            <span className="total">{props.treated}</span>
        </div>
    );
};

Statistics.propTypes = {
    treated: React.PropTypes.number.isRequired,
    valid: React.PropTypes.number.isRequired
};

export default Statistics;