import React, {PropTypes} from 'react';
import Feed from './Feed';
import Statistics from './Statistics';

const MainPage = (props) => {

    return (
        <section id="main">
            <div className="row">
                <div className="col-md-12">
                    <Statistics treated={props.feed.nbTransactionTreated} valid={props.feed.nbTransactionValid} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <Feed feed={props.feed.data} />
                </div>
            </div>
        </section>
    );
};

MainPage.propTypes = {
    feed: React.PropTypes.shape({
        nbTransactionTreated: React.PropTypes.number.isRequired,
        nbTransactionValid: React.PropTypes.number.isRequired,
        data: React.PropTypes.array.isRequired
    })
};

export default MainPage;