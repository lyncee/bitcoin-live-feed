import * as constants from '../constants/constants';
import * as DateHelper from '../tools/DateHelper';

function getSum(total, item) {
    return  parseInt(total.value) + parseInt(item.value);
}

function calculateTotal(transactionOutputs) {
    return transactionOutputs.reduce(getSum);
}

function filterSmallValue(value) {
    return value > constants.threshold;
}

function convertSatoshiToBitcoin(value) {
    return value * constants.satoshiRatio;
}

export function filterData(data) {
    // We exclude the ping responses by filtering the op UTX
    if(data.op != "utx") { return; }
    // We calculate the transaction total amount
    let totalAmount = convertSatoshiToBitcoin(calculateTotal(data.x.out));
    // We filter if the amount is lesser than our threshold
    if(!filterSmallValue(totalAmount)){ return; }
    // We parse and return the object that we want in our state
    let transactions = [];
    data.x.out.map(function(item) {
        transactions.push({ "address": item.addr, "amount": convertSatoshiToBitcoin(item.value) });
    });

    return {
        "time": DateHelper.convertEpochToDate(data.x.time),
        "total": totalAmount,
        "transactions": transactions
    };
}