
function formatDate(date) {
    return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
}

export function convertEpochToDate(value) {
    let d = new Date(0);
    d.setUTCSeconds(value);
    return formatDate(d);
}