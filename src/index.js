/*eslint-disable import/default */

import 'babel-polyfill';
import './style/app.scss';
import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';


const redux = configureStore();

render(
	<Provider store={redux}>
		<Router history={browserHistory} routes={routes} />
	</Provider>, 
	document.getElementById("app")
);