import * as constants from './../constants/constants';


class WebSocketService {

    constructor() {
        this.connect = this.connect.bind(this);
    }

    connect(url, dispatcher) {

        const webSocket = new WebSocket(url);

        webSocket.onmessage = function (event) {
            console.log('Receiving...');
            dispatcher(JSON.parse(event.data));
        };

        webSocket.onopen = function () {
            setTimeout(function() {
                webSocket.send(JSON.stringify({"op":"unconfirmed_sub"}));
                console.log('Connected');
            }, 1000);
        };

        setTimeout(function() {
            webSocket.send(JSON.stringify({"op":"ping"}));
        }, constants.refreshDelay);

        webSocket.onclose = function() {
            console.log('Reconnecting...');
            setTimeout(function() {
                this.connect(url, dispatcher);
            }, constants.reconnectDelay);
        }
    }

}

export default WebSocketService;


