import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import MainContainer from './containers/pages/MainContainer';

export default (
	<Route path="/" component={App}>
		<IndexRoute component={MainContainer} />
	</Route>
);