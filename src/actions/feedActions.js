import * as types from '../constants/actionTypes';

export function receiveMessage(data){ return { type: types.RECEIVE_MESSAGE, data }; }