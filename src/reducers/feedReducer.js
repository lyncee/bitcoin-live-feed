import * as types from '../constants/actionTypes';
import * as Calculator from '../tools/CalculatorHelper';

const initialState = {
    nbTransactionTreated: 0,
    nbTransactionValid: 0,
    data: []
};

function feedReducer(state = initialState, action) {

    switch (action.type) {

        case types.RECEIVE_MESSAGE: {

            let result = Calculator.filterData(action.data);

            return Object.assign({}, state, {
                nbTransactionTreated: state.nbTransactionTreated + 1,
                nbTransactionValid: result != null ? state.nbTransactionValid + 1 : state.nbTransactionValid,
                data: result != null ? [result, ...state.data] : state.data
            });
        }

        default:
            return state;
    }
}

export default feedReducer;