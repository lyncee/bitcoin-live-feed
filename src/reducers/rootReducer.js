// Root reducer
import {combineReducers} from 'redux';
import feed from './feedReducer';

const rootReducer = combineReducers({
    feed: feed
});

export default rootReducer;