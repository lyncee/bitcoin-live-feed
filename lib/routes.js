'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

var _App = require('./containers/App');

var _App2 = _interopRequireDefault(_App);

var _AboutContainer = require('./containers/pages/AboutContainer');

var _AboutContainer2 = _interopRequireDefault(_AboutContainer);

var _ContactContainer = require('./containers/pages/ContactContainer');

var _ContactContainer2 = _interopRequireDefault(_ContactContainer);

var _ProjectsContainer = require('./containers/pages/ProjectsContainer');

var _ProjectsContainer2 = _interopRequireDefault(_ProjectsContainer);

var _ReviewsContainer = require('./containers/pages/ReviewsContainer');

var _ReviewsContainer2 = _interopRequireDefault(_ReviewsContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _react2.default.createElement(
	_reactRouter.Route,
	{ path: '/', component: _App2.default },
	_react2.default.createElement(_reactRouter.IndexRoute, { component: _ProjectsContainer2.default }),
	_react2.default.createElement(_reactRouter.Route, { path: 'contact', component: _ContactContainer2.default }),
	_react2.default.createElement(_reactRouter.Route, { path: 'reviews', component: _ReviewsContainer2.default }),
	_react2.default.createElement(_reactRouter.Route, { path: 'about', component: _AboutContainer2.default })
);