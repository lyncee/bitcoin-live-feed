'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _redux = require('redux');

var _contactActions = require('../../actions/contactActions');

var contactActions = _interopRequireWildcard(_contactActions);

var _ContactPage = require('../../components/contact/ContactPage');

var _ContactPage2 = _interopRequireDefault(_ContactPage);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ContactContainer = function (_React$Component) {
    _inherits(ContactContainer, _React$Component);

    function ContactContainer(props, context) {
        _classCallCheck(this, ContactContainer);

        var _this = _possibleConstructorReturn(this, (ContactContainer.__proto__ || Object.getPrototypeOf(ContactContainer)).call(this, props, context));

        _this.state = {
            email: {
                name: '',
                address: '',
                subject: 'Commission request',
                message: ''
            },
            errors: {}
        };

        _this.subjects = ["General enquiries", "Commission request", "Other"];
        _this.onChange = _this.onChange.bind(_this);
        _this.onClick = _this.onClick.bind(_this);
        return _this;
    }

    _createClass(ContactContainer, [{
        key: 'onChange',
        value: function onChange(event) {

            var email = this.state.email;

            switch (event.target.name) {
                case 'name':
                    email.name = event.target.value;
                    break;
                case 'address':
                    email.address = event.target.value;
                    break;
                case 'subject':
                    email.subject = event.target.value;
                    break;
                case 'message':
                    email.message = event.target.value;
                    break;
            }

            this.setState({ email: email });
        }
    }, {
        key: 'onClick',
        value: function onClick(event) {
            event.preventDefault();

            if (!this.validateForm()) {
                return;
            }

            this.props.actions.sendEmail(this.state.email);
            $(event.target).prop("disabled", true);
        }
    }, {
        key: 'validateForm',
        value: function validateForm() {
            var formIsValid = true;
            var errors = {};

            if (this.state.email.name == '') {
                errors.name = 'Name can not be empty';
                formIsValid = false;
            }

            var regex = /\S+@\S+\.\S+/;
            if (!regex.test(this.state.email.address)) {
                errors.address = 'Email is invalid';
                formIsValid = false;
            }

            if (this.state.email.message == '') {
                errors.message = 'Message can not be empty';
                formIsValid = false;
            }

            if (this.state.email.message.length < 20) {
                errors.message = 'Message must be at least 20 characters';
                formIsValid = false;
            }

            this.setState({
                errors: errors
            });

            return formIsValid;
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(_ContactPage2.default, { email: this.state.email, errors: this.state.errors, subjects: this.subjects, onChange: this.onChange, onClick: this.onClick });
        }
    }]);

    return ContactContainer;
}(_react2.default.Component);

ContactContainer.propTypes = {
    actions: _react.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        email: state.email,
        errors: state.errors
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: (0, _redux.bindActionCreators)(contactActions, dispatch)
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ContactContainer);