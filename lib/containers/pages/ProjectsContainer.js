'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _redux = require('redux');

var _projectActions = require('../../actions/projectActions');

var projectActions = _interopRequireWildcard(_projectActions);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _ProjectsPage = require('../../components/projects/ProjectsPage');

var _ProjectsPage2 = _interopRequireDefault(_ProjectsPage);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProjectsContainer = function (_React$Component) {
    _inherits(ProjectsContainer, _React$Component);

    function ProjectsContainer(props, context) {
        _classCallCheck(this, ProjectsContainer);

        var _this = _possibleConstructorReturn(this, (ProjectsContainer.__proto__ || Object.getPrototypeOf(ProjectsContainer)).call(this, props, context));

        _this.showMore = _this.showMore.bind(_this);
        return _this;
    }

    _createClass(ProjectsContainer, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.props.actions.getProjects();
        }
    }, {
        key: 'showMore',
        value: function showMore(event) {
            var $button = (0, _jquery2.default)(event.target).closest('.options');
            var $details = $button.closest('.project').find('.details');

            if ($button.hasClass('selected')) {
                $button.removeClass('selected');
                $details.addClass('hidden');
            } else {
                $button.addClass('selected');
                $details.removeClass('hidden');
            }
        }
    }, {
        key: 'render',
        value: function render() {

            return _react2.default.createElement(_ProjectsPage2.default, { projects: this.props.projects, showMore: this.showMore });
        }
    }]);

    return ProjectsContainer;
}(_react2.default.Component);

ProjectsContainer.propTypes = {
    projects: _react.PropTypes.array.isRequired,
    actions: _react.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        projects: state.projects
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: (0, _redux.bindActionCreators)(projectActions, dispatch)
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ProjectsContainer);