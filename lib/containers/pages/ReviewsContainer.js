'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _redux = require('redux');

var _reviewActions = require('../../actions/reviewActions');

var reviewActions = _interopRequireWildcard(_reviewActions);

var _ReviewsPage = require('../../components/reviews/ReviewsPage');

var _ReviewsPage2 = _interopRequireDefault(_ReviewsPage);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReviewsContainer = function (_React$Component) {
    _inherits(ReviewsContainer, _React$Component);

    function ReviewsContainer(props, context) {
        _classCallCheck(this, ReviewsContainer);

        var _this = _possibleConstructorReturn(this, (ReviewsContainer.__proto__ || Object.getPrototypeOf(ReviewsContainer)).call(this, props, context));

        _this.state = {
            review: {
                comment: '',
                company: '',
                name: '',
                role: '',
                createdAt: ''
            },
            errors: {}
        };

        _this.onChange = _this.onChange.bind(_this);
        _this.onClick = _this.onClick.bind(_this);
        return _this;
    }

    _createClass(ReviewsContainer, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            this.props.actions.getReviews();
        }
    }, {
        key: 'onChange',
        value: function onChange(event) {

            var review = this.state.review;

            switch (event.target.name) {
                case 'comment':
                    review.comment = event.target.value;
                    break;
                case 'company':
                    review.company = event.target.value;
                    break;
                case 'name':
                    review.name = event.target.value;
                    break;
                case 'role':
                    review.role = event.target.value;
                    break;
            }

            this.setState({ review: review });
        }
    }, {
        key: 'onClick',
        value: function onClick(event) {
            event.preventDefault();

            if (!this.validateForm()) {
                return;
            }

            this.props.actions.createReview(this.state.review);
            (0, _jquery2.default)(event.target).prop("disabled", true);
        }
    }, {
        key: 'validateForm',
        value: function validateForm() {
            // Init variables
            var formIsValid = true;
            var errors = {};

            if (this.state.review.name == '') {
                errors.name = 'Name can not be empty';
                formIsValid = false;
            }

            if (this.state.review.comment == '') {
                errors.comment = 'Comment can not be empty';
                formIsValid = false;
            }

            if (this.state.review.comment.length < 20) {
                errors.comment = 'Comment must be at least 20 characters';
                formIsValid = false;
            }

            this.setState({ errors: errors });

            return formIsValid;
        }
    }, {
        key: 'render',
        value: function render() {

            return _react2.default.createElement(_ReviewsPage2.default, { review: this.state.review, errors: this.state.errors, onChange: this.onChange, onClick: this.onClick, reviews: this.props.reviews });
        }
    }]);

    return ReviewsContainer;
}(_react2.default.Component);

ReviewsContainer.defaultProps = {
    reviews: []
};

ReviewsContainer.propTypes = {
    reviews: _react.PropTypes.array.isRequired,
    actions: _react.PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        reviews: state.reviews,
        review: state.review,
        errors: state.errors
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: (0, _redux.bindActionCreators)(reviewActions, dispatch)
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ReviewsContainer);