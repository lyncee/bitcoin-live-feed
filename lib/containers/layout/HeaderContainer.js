'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _redux = require('redux');

var _applicationActions = require('../../actions/applicationActions');

var applicationActions = _interopRequireWildcard(_applicationActions);

var _Header = require('../../components/layout/Header');

var _Header2 = _interopRequireDefault(_Header);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HeaderContainer = function (_React$Component) {
    _inherits(HeaderContainer, _React$Component);

    function HeaderContainer(props, context) {
        _classCallCheck(this, HeaderContainer);

        var _this = _possibleConstructorReturn(this, (HeaderContainer.__proto__ || Object.getPrototypeOf(HeaderContainer)).call(this, props, context));

        _this.collapseMenu = _this.collapseMenu.bind(_this);
        return _this;
    }

    _createClass(HeaderContainer, [{
        key: 'collapseMenu',
        value: function collapseMenu(event) {
            $(event.target).closest(".navbar-collapse").collapse('hide');
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(_Header2.default, { isFetching: this.props.isFetching, collapseMenu: this.collapseMenu });
        }
    }]);

    return HeaderContainer;
}(_react2.default.Component);

HeaderContainer.propTypes = {
    isFetching: _react.PropTypes.bool.isRequired
};

HeaderContainer.defaultProps = {
    isFetching: false
};

function mapStateToProps(state, ownProps) {
    return {
        isFetching: state.application.isFetching
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: (0, _redux.bindActionCreators)(applicationActions, dispatch)
    };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(HeaderContainer);