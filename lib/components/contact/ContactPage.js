'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _SendEmailForm = require('./SendEmailForm');

var _SendEmailForm2 = _interopRequireDefault(_SendEmailForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ContactPage = function ContactPage(props) {

    var linkedInUrl = "https://www.linkedin.com/in/guillaume-marquilly-0403945b";

    return _react2.default.createElement(
        'section',
        { id: 'contact' },
        _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
                'div',
                { className: 'col-md-6 col-sd-12' },
                _react2.default.createElement(
                    'div',
                    { className: 'panel panel-default' },
                    _react2.default.createElement(
                        'div',
                        { className: 'panel-body' },
                        _react2.default.createElement(
                            'p',
                            { className: 'title' },
                            'Contact'
                        ),
                        _react2.default.createElement(
                            'p',
                            null,
                            'You can reach my profile on ',
                            _react2.default.createElement(
                                'a',
                                { className: 'linkedin', href: linkedInUrl, target: '_blank' },
                                'LinkedIn'
                            )
                        ),
                        _react2.default.createElement(
                            'p',
                            null,
                            'Otherwise, the best way to get in touch with me is to use this form.'
                        ),
                        _react2.default.createElement(
                            'p',
                            { className: 'justified' },
                            'Please we aware that I only accept commissioned work on occasion depending on my schedule.'
                        ),
                        _react2.default.createElement(_SendEmailForm2.default, {
                            email: props.email,
                            errors: props.errors,
                            subjects: props.subjects,
                            onChange: props.onChange,
                            onClick: props.onClick })
                    )
                )
            )
        )
    );
};

ContactPage.propTypes = {
    email: _react.PropTypes.object.isRequired,
    errors: _react.PropTypes.object.isRequired,
    subjects: _react.PropTypes.array.isRequired,
    onChange: _react.PropTypes.func.isRequired,
    onClick: _react.PropTypes.func.isRequired
};

exports.default = ContactPage;