'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('../form/Button');

var _Button2 = _interopRequireDefault(_Button);

var _TextArea = require('../form/TextArea');

var _TextArea2 = _interopRequireDefault(_TextArea);

var _TextBox = require('../form/TextBox');

var _TextBox2 = _interopRequireDefault(_TextBox);

var _SingleSelect = require('../form/SingleSelect');

var _SingleSelect2 = _interopRequireDefault(_SingleSelect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SendEmailForm = function SendEmailForm(props) {

    var mandatory = true;

    return _react2.default.createElement(
        'form',
        null,
        _react2.default.createElement(_TextBox2.default, {
            name: 'name',
            label: 'Your Name',
            mandatory: mandatory,
            value: props.email.name,
            error: props.errors.name,
            onChange: props.onChange }),
        _react2.default.createElement(_TextBox2.default, {
            name: 'address',
            label: 'Your Email Address',
            mandatory: mandatory,
            value: props.email.address,
            error: props.errors.address,
            onChange: props.onChange }),
        _react2.default.createElement(_SingleSelect2.default, {
            name: 'subject',
            label: 'Contact Reason',
            options: props.subjects,
            value: props.email.subject,
            error: props.errors.subject,
            onChange: props.onChange }),
        _react2.default.createElement(_TextArea2.default, {
            name: 'message',
            label: 'Your Message',
            mandatory: mandatory,
            value: props.email.message,
            error: props.errors.message,
            onChange: props.onChange }),
        _react2.default.createElement(_Button2.default, {
            label: 'Send message',
            onClick: props.onClick })
    );
};

SendEmailForm.propTypes = {
    email: _react.PropTypes.object.isRequired,
    errors: _react.PropTypes.object.isRequired,
    subjects: _react.PropTypes.array.isRequired,
    onChange: _react.PropTypes.func.isRequired,
    onClick: _react.PropTypes.func.isRequired
};

exports.default = SendEmailForm;