"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AboutPage = function AboutPage() {

    return _react2.default.createElement(
        "section",
        { id: "about" },
        _react2.default.createElement(
            "div",
            { className: "row" },
            _react2.default.createElement(
                "div",
                { className: "col-md-6 col-sd-12" },
                _react2.default.createElement(
                    "div",
                    { className: "panel panel-default" },
                    _react2.default.createElement(
                        "div",
                        { className: "panel-body" },
                        _react2.default.createElement(
                            "p",
                            { className: "title" },
                            "Professional History"
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "I started working in France in 2008 for ",
                            _react2.default.createElement(
                                "strong",
                                null,
                                "Scotler"
                            ),
                            ", a Computer Services Company, as a consultant specialising in SharePoint solutions. This experience allowed me to gain great experience in SharePoint as well as .NET & front-end development. You could say it really began my love affair with tech. Most importantly, I worked with customers who weren't tech literate and so forced me to learn how to translate their requirements into software solutions."
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "In 2011, I moved to Hong Kong for a long-term mission. ",
                            _react2.default.createElement(
                                "strong",
                                null,
                                "TV5 Monde"
                            ),
                            " a french TV channel, hired me in order to review and renew their entire South Asian web presence. The role was very challenging particularly as I was the only software engineer. However, I soon successfully produced 4 new multilingual websites based on Kentico .NET."
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "Around the same time, I also started a personal project named ",
                            _react2.default.createElement(
                                "strong",
                                null,
                                "REGIF.NET"
                            ),
                            ", a social network to create and share .GIF pictures. The most interesting part was a feature to customize .GIFs. Using vanilla javascript, this feature was able to decrypt any GIF file in order to split each frame in a gallery. It was then possible to apply pixels transformations on each frame using HTML5 Canvas, and finally re-encrypting everything back into a .GIF format."
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "In 2015, I decided to come back to Europe. I moved to the UK and started a new job at ",
                            _react2.default.createElement(
                                "strong",
                                null,
                                "Capital on Tap"
                            ),
                            ", a small but successful FinTech company. I had a great time working in and addressing the unique challenges involved in a startup environment."
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "This year, I decided to continue my self-learning and also help businesses by providing guidance and best practices on various technologies."
                        )
                    )
                ),
                _react2.default.createElement(
                    "div",
                    { className: "panel panel-default" },
                    _react2.default.createElement(
                        "div",
                        { className: "panel-body" },
                        _react2.default.createElement(
                            "p",
                            { className: "title" },
                            "Centre of Interests"
                        ),
                        _react2.default.createElement(
                            "ul",
                            null,
                            _react2.default.createElement(
                                "li",
                                null,
                                "Programming"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Learning"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Videogames (Retrogaming, OverWatch, LoL)"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Anime & TV Series"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Travelling"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Cooking"
                            )
                        )
                    )
                )
            ),
            _react2.default.createElement(
                "div",
                { className: "col-md-6 col-sd-12" },
                _react2.default.createElement(
                    "div",
                    { className: "panel panel-default" },
                    _react2.default.createElement(
                        "div",
                        { className: "panel-body" },
                        _react2.default.createElement(
                            "p",
                            { className: "title" },
                            "About this website"
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "Early 2017, I created this website in order to promote my projects. It was also the perfect opportunity to develop a web application in React using the following key points :"
                        ),
                        _react2.default.createElement(
                            "ul",
                            null,
                            _react2.default.createElement(
                                "li",
                                null,
                                "React ES6"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Redux Saga"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                ".NET Core API"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Sass"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Bootstrap"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Webpack"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Webstorm"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "AWS"
                            )
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "justified" },
                            "If you wish to know more about it, I also released the source code at this GIT repository address : ",
                            _react2.default.createElement(
                                "a",
                                { href: "https://gitlab.com/lyncee/behelit", target: "_blank" },
                                "https://gitlab.com/lyncee/behelit"
                            ),
                            "."
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "title" },
                            "Workstation"
                        ),
                        _react2.default.createElement(
                            "ul",
                            null,
                            _react2.default.createElement(
                                "li",
                                null,
                                "Intel Core i7 6700K @4GHz"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Asus Z170 Pro Gaming"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Corsair Vengeance PC24000 3GHz 16GB DDR4 x2"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Corsair Hydro Series H60 2013"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "MSI GeForce GTX 1060 3GB DDR5"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "SanDisk Ultra II 480GB SATA III"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Evga Supernova 650 G3"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "DELL P2417H 23.8\" x4"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Roccat ISKU FX"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Roccat KONE XTD"
                            )
                        ),
                        _react2.default.createElement(
                            "p",
                            { className: "title" },
                            "Beloved Software"
                        ),
                        _react2.default.createElement(
                            "ul",
                            null,
                            _react2.default.createElement(
                                "li",
                                null,
                                "Microsoft Visual Studio 2015 Update 3"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "JetBrains ReSharper"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "JetBrains WebStorm"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "JetBrains Rider"
                            ),
                            _react2.default.createElement(
                                "li",
                                null,
                                "Paint .NET"
                            )
                        )
                    )
                )
            )
        )
    );
};

exports.default = AboutPage;