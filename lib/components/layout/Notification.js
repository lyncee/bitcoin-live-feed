"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Notification = function Notification(props) {

    var notificationClass = "notification ";
    notificationClass += props.type;
    if (!props.isDisplayed) {
        notificationClass += " hidden";
    }

    return _react2.default.createElement(
        "span",
        { className: notificationClass },
        props.message
    );
};

Notification.propTypes = {
    message: _react.PropTypes.string.isRequired,
    type: _react.PropTypes.string.isRequired,
    isDisplayed: _react.PropTypes.bool.isRequired
};

exports.default = Notification;