'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Header = function Header(props) {

    var loaderClass = props.isFetching ? "loader" : "loader hidden";

    return _react2.default.createElement(
        'nav',
        { className: 'navbar navbar-fixed-top' },
        _react2.default.createElement(
            'div',
            { className: 'container' },
            _react2.default.createElement(
                'div',
                { className: 'navbar-header' },
                _react2.default.createElement(
                    'button',
                    { type: 'button', className: 'navbar-toggle collapsed', 'data-toggle': 'collapse', 'data-target': '.navbar-collapse', 'aria-expanded': 'false' },
                    _react2.default.createElement(
                        'span',
                        { className: 'sr-only' },
                        'Toggle navigation'
                    ),
                    _react2.default.createElement('span', { className: 'icon-bar' }),
                    _react2.default.createElement('span', { className: 'icon-bar' }),
                    _react2.default.createElement('span', { className: 'icon-bar' })
                ),
                _react2.default.createElement(
                    'a',
                    { className: 'navbar-brand', href: '/' },
                    _react2.default.createElement(
                        'div',
                        { className: 'logo' },
                        'Behelit'
                    )
                )
            ),
            _react2.default.createElement(
                'div',
                { className: 'navbar-collapse collapse' },
                _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav' },
                    _react2.default.createElement(
                        'li',
                        null,
                        _react2.default.createElement(
                            _reactRouter.IndexLink,
                            { to: '/', activeClassName: 'active', onClick: props.collapseMenu },
                            'Projects'
                        )
                    ),
                    _react2.default.createElement(
                        'li',
                        null,
                        _react2.default.createElement(
                            _reactRouter.Link,
                            { to: '/main', activeClassName: 'active', onClick: props.collapseMenu },
                            'Reviews'
                        )
                    ),
                    _react2.default.createElement(
                        'li',
                        null,
                        _react2.default.createElement(
                            _reactRouter.Link,
                            { to: '/contact', activeClassName: 'active', onClick: props.collapseMenu },
                            'Contact'
                        )
                    ),
                    _react2.default.createElement(
                        'li',
                        null,
                        _react2.default.createElement(
                            _reactRouter.Link,
                            { to: '/about', activeClassName: 'active', onClick: props.collapseMenu },
                            'About'
                        )
                    )
                ),
                _react2.default.createElement(
                    'ul',
                    { className: 'nav navbar-nav navbar-right' },
                    _react2.default.createElement(
                        'div',
                        { className: loaderClass },
                        'Loading...'
                    )
                )
            )
        )
    );
};

Header.propTypes = {
    isFetching: _react.PropTypes.bool.isRequired,
    collapseMenu: _react.PropTypes.func.isRequired
};

exports.default = Header;