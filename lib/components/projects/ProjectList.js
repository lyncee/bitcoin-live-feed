'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _TagList = require('./TagList');

var _TagList2 = _interopRequireDefault(_TagList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProjectList = function ProjectList(props) {

    return _react2.default.createElement(
        'div',
        { className: 'projects' },
        props.projects.map(function (project, index) {

            return _react2.default.createElement(
                'div',
                { key: index, className: 'project' },
                _react2.default.createElement(
                    'div',
                    { className: 'row' },
                    _react2.default.createElement(
                        'div',
                        { className: 'col-md-12 col-sm-12' },
                        _react2.default.createElement(
                            'p',
                            { className: 'title' },
                            project.title
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'row' },
                    _react2.default.createElement(
                        'div',
                        { className: 'col-md-12 col-sm-12' },
                        _react2.default.createElement(
                            'p',
                            { className: 'company' },
                            project.company,
                            ' - ',
                            project.year
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'row' },
                    _react2.default.createElement(
                        'div',
                        { className: 'col-md-7 col-sm-6' },
                        _react2.default.createElement('p', { className: 'justified', dangerouslySetInnerHTML: { __html: project.summary } })
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'col-md-5 col-sm-6' },
                        _react2.default.createElement(_TagList2.default, { tags: project.tags })
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'row' },
                    _react2.default.createElement(
                        'div',
                        { className: 'col-md-12 col-sm-12' },
                        _react2.default.createElement(
                            'div',
                            { className: 'options', onClick: props.showMore },
                            _react2.default.createElement('i', { className: 'fa fa-arrow-circle-down' }),
                            _react2.default.createElement(
                                'span',
                                null,
                                'More'
                            )
                        )
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'row padding-top-20 details hidden' },
                    _react2.default.createElement(
                        'div',
                        { className: 'col-md-12 col-sm-12' },
                        _react2.default.createElement('p', { className: 'justified',
                            dangerouslySetInnerHTML: { __html: project.description } })
                    )
                )
            );
        })
    );
};

ProjectList.propTypes = {
    projects: _react.PropTypes.array.isRequired,
    showMore: _react.PropTypes.func.isRequired
};

exports.default = ProjectList;