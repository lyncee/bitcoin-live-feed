'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ProjectList = require('./ProjectList');

var _ProjectList2 = _interopRequireDefault(_ProjectList);

var _avatar = require('../../style/images/avatar.png');

var _avatar2 = _interopRequireDefault(_avatar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProjectsPage = function ProjectsPage(props) {

    return _react2.default.createElement(
        'section',
        { id: 'projects' },
        _react2.default.createElement(
            'div',
            { className: 'row padding-bottom-10' },
            _react2.default.createElement(
                'div',
                { className: 'col-lg-3 col-md-3 hidden-sm hidden-xs' },
                _react2.default.createElement('img', { className: 'avatar', src: _avatar2.default })
            ),
            _react2.default.createElement(
                'div',
                { className: 'col-lg-9 col-md-9 col-sm-12 col-xs-12' },
                _react2.default.createElement(
                    'div',
                    { className: 'citation' },
                    _react2.default.createElement('i', { className: 'fa fa-quote-left', 'aria-hidden': 'true' }),
                    _react2.default.createElement(
                        'p',
                        { className: 'justified' },
                        'I\'m a full stack developer with over 7 years of experience working in .NET HTML5/CSS3/Javascript, but in recent years specialised in the front-end.'
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        'I\'m curious and resourceful, and I like to spend my spare time learning and increasing my knowledge. I began my career as a project manager and but more recently worked as a developer in an Agile team utilising the XP methodology. I\'m interested in roles where I will meet challenges but also relish the opportunity to learn and teach.'
                    ),
                    _react2.default.createElement(
                        'p',
                        null,
                        'I am also interested in developing websites for any businesses looking to use new technologies and best practices.'
                    ),
                    _react2.default.createElement('i', { className: 'fa fa-quote-right', 'aria-hidden': 'true' })
                )
            )
        ),
        _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
                'div',
                { className: 'col-md-12 col-sd-12' },
                _react2.default.createElement(
                    'div',
                    { className: 'panel panel-default' },
                    _react2.default.createElement(
                        'div',
                        { className: 'panel-body' },
                        _react2.default.createElement(_ProjectList2.default, { projects: props.projects, showMore: props.showMore })
                    )
                )
            )
        )
    );
};

ProjectsPage.propTypes = {
    projects: _react.PropTypes.array.isRequired,
    showMore: _react.PropTypes.func.isRequired
};

exports.default = ProjectsPage;