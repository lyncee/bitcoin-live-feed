"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TagList = function TagList(props) {

    return _react2.default.createElement(
        "p",
        { className: "justified" },
        props.tags.map(function (tag, index) {
            return _react2.default.createElement(
                "span",
                { key: index, className: index % 2 == 0 ? "tag even" : "tag odd" },
                tag
            );
        })
    );
};

TagList.propTypes = {
    tags: _react.PropTypes.array.isRequired
};

exports.default = TagList;