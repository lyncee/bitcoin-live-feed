'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('../form/Button');

var _Button2 = _interopRequireDefault(_Button);

var _TextArea = require('../form/TextArea');

var _TextArea2 = _interopRequireDefault(_TextArea);

var _TextBox = require('../form/TextBox');

var _TextBox2 = _interopRequireDefault(_TextBox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CreateReviewForm = function CreateReviewForm(props) {

	var mandatory = true;

	return _react2.default.createElement(
		'form',
		null,
		_react2.default.createElement(_TextBox2.default, {
			name: 'name',
			label: 'Name',
			mandatory: mandatory,
			value: props.review.name,
			error: props.errors.name,
			onChange: props.onChange }),
		_react2.default.createElement(_TextBox2.default, {
			name: 'company',
			label: 'Company',
			value: props.review.company,
			onChange: props.onChange }),
		_react2.default.createElement(_TextBox2.default, {
			name: 'role',
			label: 'Role',
			value: props.review.role,
			onChange: props.onChange }),
		_react2.default.createElement(_TextArea2.default, {
			name: 'comment',
			label: 'Comment',
			mandatory: mandatory,
			value: props.review.comment,
			error: props.errors.comment,
			onChange: props.onChange }),
		_react2.default.createElement(_Button2.default, {
			label: 'Submit review',
			onClick: props.onClick })
	);
};

CreateReviewForm.propTypes = {
	review: _react.PropTypes.object.isRequired,
	errors: _react.PropTypes.object.isRequired,
	onChange: _react.PropTypes.func.isRequired,
	onClick: _react.PropTypes.func.isRequired
};

exports.default = CreateReviewForm;