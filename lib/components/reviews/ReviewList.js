"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ReviewList = function ReviewList(props) {

    return _react2.default.createElement(
        "div",
        { className: "reviews" },
        props.reviews.map(function (review, index) {

            var companyStyle = "company";
            if (!review.company) {
                companyStyle = ' ' + "hidden";
            }

            var roleStyle = "role";
            if (!review.role) {
                roleStyle = ' ' + "hidden";
            }

            return _react2.default.createElement(
                "div",
                { key: index, className: "review" },
                _react2.default.createElement(
                    "div",
                    { className: "citation" },
                    _react2.default.createElement("i", { className: "fa fa-quote-left", "aria-hidden": "true" }),
                    _react2.default.createElement(
                        "p",
                        { className: "justified" },
                        review.comment
                    ),
                    _react2.default.createElement("i", { className: "fa fa-quote-right", "aria-hidden": "true" })
                ),
                _react2.default.createElement(
                    "div",
                    { className: "info" },
                    _react2.default.createElement(
                        "div",
                        { className: "row" },
                        _react2.default.createElement(
                            "div",
                            { className: "col-md-12 col-sd-12" },
                            _react2.default.createElement(
                                "span",
                                { className: "name" },
                                review.name
                            ),
                            _react2.default.createElement(
                                "span",
                                { className: "date" },
                                review.createdAt
                            )
                        )
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "row" },
                        _react2.default.createElement(
                            "div",
                            { className: "col-md-12 col-sd-12" },
                            _react2.default.createElement(
                                "span",
                                { className: roleStyle },
                                review.role
                            ),
                            _react2.default.createElement(
                                "span",
                                { className: companyStyle },
                                " @ ",
                                review.company
                            )
                        )
                    )
                )
            );
        })
    );
};

ReviewList.propTypes = {
    reviews: _react.PropTypes.array.isRequired
};

exports.default = ReviewList;