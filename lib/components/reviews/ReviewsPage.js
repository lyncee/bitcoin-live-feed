'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CreateReviewForm = require('./CreateReviewForm');

var _CreateReviewForm2 = _interopRequireDefault(_CreateReviewForm);

var _ReviewList = require('./ReviewList');

var _ReviewList2 = _interopRequireDefault(_ReviewList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ReviewsPage = function ReviewsPage(props) {

    return _react2.default.createElement(
        'section',
        { id: 'reviews' },
        _react2.default.createElement(
            'div',
            { className: 'row' },
            _react2.default.createElement(
                'div',
                { className: 'col-md-6 col-sd-12' },
                _react2.default.createElement(
                    'div',
                    { className: 'panel panel-default' },
                    _react2.default.createElement(
                        'div',
                        { className: 'panel-body' },
                        _react2.default.createElement(
                            'p',
                            { className: 'title' },
                            'Reviews'
                        ),
                        _react2.default.createElement(
                            'p',
                            { className: 'justified' },
                            'If we had the chance to work together, don\'t hesitate to write a review !'
                        ),
                        _react2.default.createElement(_CreateReviewForm2.default, {
                            review: props.review,
                            errors: props.errors,
                            onChange: props.onChange,
                            onClick: props.onClick })
                    )
                )
            ),
            _react2.default.createElement(
                'div',
                { className: 'col-md-6 col-sd-12' },
                _react2.default.createElement(_ReviewList2.default, { reviews: props.reviews })
            )
        )
    );
};

ReviewsPage.propTypes = {
    review: _react.PropTypes.object.isRequired,
    reviews: _react.PropTypes.array.isRequired,
    errors: _react.PropTypes.object.isRequired,
    onChange: _react.PropTypes.func.isRequired,
    onClick: _react.PropTypes.func.isRequired
};

exports.default = ReviewsPage;