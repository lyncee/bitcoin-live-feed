'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TextArea = function TextArea(props) {

	var wrapperClass = 'form-group';
	if (props.error && props.error.length > 0) {
		wrapperClass += ' ' + 'has-error';
	}

	var labelClass = '';
	if (props.mandatory) {
		labelClass += 'required';
	}

	return _react2.default.createElement(
		'div',
		{ className: wrapperClass },
		_react2.default.createElement(
			'label',
			{ className: labelClass, htmlFor: props.name },
			props.label
		),
		_react2.default.createElement(
			'div',
			{ className: 'field' },
			_react2.default.createElement('textarea', {
				className: 'form-control',
				name: props.name,
				placeholder: props.placeholder,
				value: props.value,
				rows: props.rows,
				required: props.mandatory,
				onChange: props.onChange }),
			_react2.default.createElement(
				'div',
				{ className: 'input' },
				props.error
			)
		)
	);
};

TextArea.defaultProps = {
	mandatory: false,
	rows: 5
};

TextArea.propTypes = {
	name: _react2.default.PropTypes.string.isRequired,
	label: _react2.default.PropTypes.string.isRequired,
	onChange: _react2.default.PropTypes.func.isRequired,
	mandatory: _react2.default.PropTypes.bool.isRequired,
	rows: _react2.default.PropTypes.number,
	placeholder: _react2.default.PropTypes.string,
	value: _react2.default.PropTypes.string,
	error: _react2.default.PropTypes.string
};

exports.default = TextArea;