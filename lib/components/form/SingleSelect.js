'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SingleSelect = function SingleSelect(props) {

    var wrapperClass = 'form-group';
    if (props.error && props.error.length > 0) {
        wrapperClass += ' ' + 'has-error';
    }

    var labelClass = '';
    if (props.mandatory) {
        labelClass += 'required';
    }

    return _react2.default.createElement(
        'div',
        { className: wrapperClass },
        _react2.default.createElement(
            'label',
            { className: labelClass, htmlFor: props.name },
            props.label
        ),
        _react2.default.createElement(
            'div',
            { className: 'field' },
            _react2.default.createElement(
                'select',
                { className: 'form-control', onChange: props.onChange, name: props.name, value: props.value },
                props.options.map(function (option, index) {
                    return _react2.default.createElement(
                        'option',
                        { key: index, value: option },
                        option
                    );
                })
            ),
            _react2.default.createElement(
                'div',
                { className: 'input' },
                props.error
            )
        )
    );
};

SingleSelect.defaultProps = {
    mandatory: false
};

SingleSelect.propTypes = {
    name: _react2.default.PropTypes.string.isRequired,
    label: _react2.default.PropTypes.string.isRequired,
    options: _react2.default.PropTypes.array.isRequired,
    onChange: _react2.default.PropTypes.func.isRequired,
    mandatory: _react2.default.PropTypes.bool.isRequired,
    value: _react2.default.PropTypes.string,
    error: _react2.default.PropTypes.string
};

exports.default = SingleSelect;