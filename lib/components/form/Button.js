"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function Button(props) {
	return _react2.default.createElement(
		"button",
		{ className: "form-control btn btn-default", onClick: props.onClick },
		props.label
	);
};

Button.propTypes = {
	onClick: _react2.default.PropTypes.func.isRequired,
	label: _react2.default.PropTypes.string.isRequired
};

exports.default = Button;