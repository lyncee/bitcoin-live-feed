'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = configureStore;

var _redux = require('redux');

var _rootReducer = require('../reducers/rootReducer');

var _rootReducer2 = _interopRequireDefault(_rootReducer);

var _reduxImmutableStateInvariant = require('redux-immutable-state-invariant');

var _reduxImmutableStateInvariant2 = _interopRequireDefault(_reduxImmutableStateInvariant);

var _reduxSaga = require('redux-saga');

var _reduxSaga2 = _interopRequireDefault(_reduxSaga);

var _sagas = require('../api/sagas');

var _sagas2 = _interopRequireDefault(_sagas);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function configureStore(initialState) {

    var immutableStateInvariant = (0, _reduxImmutableStateInvariant2.default)();
    var sagaMiddleware = (0, _reduxSaga2.default)();

    var store = (0, _redux.createStore)(_rootReducer2.default, initialState, (0, _redux.applyMiddleware)(immutableStateInvariant, sagaMiddleware));

    sagaMiddleware.run(_sagas2.default);

    return store;
}