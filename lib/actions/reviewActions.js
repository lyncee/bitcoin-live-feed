'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createReview = createReview;
exports.getReviews = getReviews;
exports.getReviewsSucceeded = getReviewsSucceeded;
exports.getReviewsFailed = getReviewsFailed;

var _actionTypes = require('./actionTypes');

var types = _interopRequireWildcard(_actionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function createReview(review) {
  return { type: types.CREATE_REVIEW, review: review };
}

function getReviews() {
  return { type: types.GET_REVIEWS, undefined: undefined };
}

function getReviewsSucceeded(reviews) {
  return { type: types.GET_REVIEWS_SUCCEEDED, reviews: reviews };
}

function getReviewsFailed(message) {
  return { type: types.GET_REVIEWS_FAILED, message: message };
}