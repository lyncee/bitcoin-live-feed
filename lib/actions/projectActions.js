'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProjects = getProjects;
exports.getProjectsSucceeded = getProjectsSucceeded;
exports.getProjectsFailed = getProjectsFailed;

var _actionTypes = require('./actionTypes');

var types = _interopRequireWildcard(_actionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function getProjects() {
  return { type: types.GET_PROJECTS };
}

function getProjectsSucceeded(projects) {
  return { type: types.GET_PROJECTS_SUCCEEDED, projects: projects };
}

function getProjectsFailed(message) {
  return { type: types.GET_PROJECTS_FAILED, message: message };
}