'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.showNotificationSuccess = showNotificationSuccess;
exports.showNotificationError = showNotificationError;
exports.hideNotification = hideNotification;

var _actionTypes = require('./actionTypes');

var types = _interopRequireWildcard(_actionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function showNotificationSuccess() {
  return { type: types.SHOW_NOTIFICATION_SUCCESS };
}

function showNotificationError() {
  return { type: types.SHOW_NOTIFICATION_ERROR };
}

function hideNotification() {
  return { type: types.HIDE_NOTIFICATION };
}