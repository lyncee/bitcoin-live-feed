'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startRequest = startRequest;
exports.stopRequest = stopRequest;

var _actionTypes = require('./actionTypes');

var types = _interopRequireWildcard(_actionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function startRequest() {
  return { type: types.START_REQUEST };
}

function stopRequest() {
  return { type: types.STOP_REQUEST };
}