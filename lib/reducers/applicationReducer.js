'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _actionTypes = require('../actions/actionTypes');

var types = _interopRequireWildcard(_actionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var initialState = {
    activeRequests: 0,
    isFetching: false
};

function applicationReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var action = arguments[1];


    switch (action.type) {

        case types.START_REQUEST:
            {
                var nbActiveRequests = state.activeRequests + 1;
                return Object.assign({}, state, {
                    activeRequests: nbActiveRequests,
                    isFetching: nbActiveRequests != 0
                });
            }

        case types.STOP_REQUEST:
            {
                var _nbActiveRequests = state.activeRequests - 1;
                return Object.assign({}, state, {
                    activeRequests: _nbActiveRequests,
                    isFetching: _nbActiveRequests != 0
                });
            }

        default:
            {
                return state;
            }
    }
}

exports.default = applicationReducer;