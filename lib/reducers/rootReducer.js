'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _redux = require('redux');

var _applicationReducer = require('./applicationReducer');

var _applicationReducer2 = _interopRequireDefault(_applicationReducer);

var _notificationReducer = require('./notificationReducer');

var _notificationReducer2 = _interopRequireDefault(_notificationReducer);

var _projectReducer = require('./projectReducer');

var _projectReducer2 = _interopRequireDefault(_projectReducer);

var _reviewReducer = require('./reviewReducer');

var _reviewReducer2 = _interopRequireDefault(_reviewReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// review => Short hand property name ES6 => equivalent to  main: main
var rootReducer = (0, _redux.combineReducers)({
    application: _applicationReducer2.default,
    notification: _notificationReducer2.default,
    projects: _projectReducer2.default,
    reviews: _reviewReducer2.default
}); // Root reducer
exports.default = rootReducer;