'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _actionTypes = require('../actions/actionTypes');

var types = _interopRequireWildcard(_actionTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var initialState = {
    message: '',
    isDisplayed: false
};

function notificationReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var action = arguments[1];


    switch (action.type) {

        case types.SHOW_NOTIFICATION_SUCCESS:
            {
                return Object.assign({}, state, {
                    message: action.message,
                    isDisplayed: true,
                    type: "success"
                });
            }

        case types.SHOW_NOTIFICATION_ERROR:
            {
                return Object.assign({}, state, {
                    message: action.message,
                    isDisplayed: true,
                    type: "error"
                });
            }

        case types.HIDE_NOTIFICATION:
            {
                return Object.assign({}, state, {
                    message: action.message,
                    isDisplayed: false
                });
            }

        default:
            {
                return state;
            }
    }
}

exports.default = notificationReducer;