'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _actionTypes = require('../actions/actionTypes');

var types = _interopRequireWildcard(_actionTypes);

var _services = require('./services');

var Services = _interopRequireWildcard(_services);

var _reduxSaga = require('redux-saga');

var _effects = require('redux-saga/effects');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = [createReview, sendEmail, getReviews, getProjects, hideNotification, sagas].map(regeneratorRuntime.mark);

function createReview(action) {
    var response;
    return regeneratorRuntime.wrap(function createReview$(_context) {
        while (1) {
            switch (_context.prev = _context.next) {
                case 0:
                    _context.prev = 0;
                    _context.next = 3;
                    return (0, _effects.put)({ type: types.START_REQUEST });

                case 3:
                    _context.next = 5;
                    return (0, _effects.call)(Services.createReview, action.review);

                case 5:
                    response = _context.sent;
                    _context.next = 8;
                    return (0, _effects.put)({ type: types.SHOW_NOTIFICATION_SUCCESS, message: response.data });

                case 8:
                    _context.next = 14;
                    break;

                case 10:
                    _context.prev = 10;
                    _context.t0 = _context['catch'](0);
                    _context.next = 14;
                    return (0, _effects.put)({ type: types.SHOW_NOTIFICATION_ERROR, message: _context.t0.message });

                case 14:
                    _context.prev = 14;
                    _context.next = 17;
                    return (0, _effects.put)({ type: types.STOP_REQUEST });

                case 17:
                    return _context.finish(14);

                case 18:
                case 'end':
                    return _context.stop();
            }
        }
    }, _marked[0], this, [[0, 10, 14, 18]]);
}

function sendEmail(action) {
    var response;
    return regeneratorRuntime.wrap(function sendEmail$(_context2) {
        while (1) {
            switch (_context2.prev = _context2.next) {
                case 0:
                    _context2.prev = 0;
                    _context2.next = 3;
                    return (0, _effects.put)({ type: types.START_REQUEST });

                case 3:
                    _context2.next = 5;
                    return (0, _effects.call)(Services.sendEmail, action.email);

                case 5:
                    response = _context2.sent;
                    _context2.next = 8;
                    return (0, _effects.put)({ type: types.SHOW_NOTIFICATION_SUCCESS, message: response.data });

                case 8:
                    _context2.next = 14;
                    break;

                case 10:
                    _context2.prev = 10;
                    _context2.t0 = _context2['catch'](0);
                    _context2.next = 14;
                    return (0, _effects.put)({ type: types.SHOW_NOTIFICATION_ERROR, message: _context2.t0.message });

                case 14:
                    _context2.prev = 14;
                    _context2.next = 17;
                    return (0, _effects.put)({ type: types.STOP_REQUEST });

                case 17:
                    return _context2.finish(14);

                case 18:
                case 'end':
                    return _context2.stop();
            }
        }
    }, _marked[1], this, [[0, 10, 14, 18]]);
}

function getReviews() {
    var response;
    return regeneratorRuntime.wrap(function getReviews$(_context3) {
        while (1) {
            switch (_context3.prev = _context3.next) {
                case 0:
                    _context3.prev = 0;
                    _context3.next = 3;
                    return (0, _effects.put)({ type: types.START_REQUEST });

                case 3:
                    _context3.next = 5;
                    return (0, _effects.call)(Services.getReviews);

                case 5:
                    response = _context3.sent;
                    _context3.t0 = response.status;
                    _context3.next = _context3.t0 === 200 ? 9 : 12;
                    break;

                case 9:
                    _context3.next = 11;
                    return (0, _effects.put)({ type: types.GET_REVIEWS_SUCCEEDED, reviews: response.data });

                case 11:
                    return _context3.abrupt('break', 14);

                case 12:
                    _context3.next = 14;
                    return (0, _effects.put)({ type: types.GET_REVIEWS_FAILED, message: response.data });

                case 14:
                    _context3.next = 20;
                    break;

                case 16:
                    _context3.prev = 16;
                    _context3.t1 = _context3['catch'](0);
                    _context3.next = 20;
                    return (0, _effects.put)({ type: types.SHOW_NOTIFICATION_ERROR, message: _context3.t1.message });

                case 20:
                    _context3.prev = 20;
                    _context3.next = 23;
                    return (0, _effects.put)({ type: types.STOP_REQUEST });

                case 23:
                    return _context3.finish(20);

                case 24:
                case 'end':
                    return _context3.stop();
            }
        }
    }, _marked[2], this, [[0, 16, 20, 24]]);
}

function getProjects() {
    var response;
    return regeneratorRuntime.wrap(function getProjects$(_context4) {
        while (1) {
            switch (_context4.prev = _context4.next) {
                case 0:
                    _context4.prev = 0;
                    _context4.next = 3;
                    return (0, _effects.put)({ type: types.START_REQUEST });

                case 3:
                    _context4.next = 5;
                    return (0, _effects.call)(Services.getProjects);

                case 5:
                    response = _context4.sent;
                    _context4.t0 = response.status;
                    _context4.next = _context4.t0 === 200 ? 9 : 12;
                    break;

                case 9:
                    _context4.next = 11;
                    return (0, _effects.put)({ type: types.GET_PROJECTS_SUCCEEDED, projects: response.data });

                case 11:
                    return _context4.abrupt('break', 14);

                case 12:
                    _context4.next = 14;
                    return (0, _effects.put)({ type: types.GET_PROJECTS_FAILED, message: response.data });

                case 14:
                    _context4.next = 20;
                    break;

                case 16:
                    _context4.prev = 16;
                    _context4.t1 = _context4['catch'](0);
                    _context4.next = 20;
                    return (0, _effects.put)({ type: types.SHOW_NOTIFICATION_ERROR, message: _context4.t1.message });

                case 20:
                    _context4.prev = 20;
                    _context4.next = 23;
                    return (0, _effects.put)({ type: types.STOP_REQUEST });

                case 23:
                    return _context4.finish(20);

                case 24:
                case 'end':
                    return _context4.stop();
            }
        }
    }, _marked[3], this, [[0, 16, 20, 24]]);
}

function hideNotification() {
    return regeneratorRuntime.wrap(function hideNotification$(_context5) {
        while (1) {
            switch (_context5.prev = _context5.next) {
                case 0:
                    _context5.next = 2;
                    return (0, _effects.call)(_reduxSaga.delay, 5000);

                case 2:
                    _context5.next = 4;
                    return (0, _effects.put)({ type: types.HIDE_NOTIFICATION });

                case 4:
                case 'end':
                    return _context5.stop();
            }
        }
    }, _marked[4], this);
}

function sagas() {
    return regeneratorRuntime.wrap(function sagas$(_context6) {
        while (1) {
            switch (_context6.prev = _context6.next) {
                case 0:
                    _context6.next = 2;
                    return (0, _reduxSaga.takeLatest)(types.CREATE_REVIEW, createReview);

                case 2:
                    _context6.next = 4;
                    return (0, _reduxSaga.takeLatest)(types.GET_REVIEWS, getReviews);

                case 4:
                    _context6.next = 6;
                    return (0, _reduxSaga.takeLatest)(types.SEND_EMAIL, sendEmail);

                case 6:
                    _context6.next = 8;
                    return (0, _reduxSaga.takeLatest)(types.GET_PROJECTS, getProjects);

                case 8:
                    _context6.next = 10;
                    return (0, _reduxSaga.takeLatest)(types.GET_REVIEWS_FAILED, hideNotification);

                case 10:
                    _context6.next = 12;
                    return (0, _reduxSaga.takeLatest)(types.GET_PROJECTS_FAILED, hideNotification);

                case 12:
                    _context6.next = 14;
                    return (0, _reduxSaga.takeLatest)(types.SHOW_NOTIFICATION_SUCCESS, hideNotification);

                case 14:
                    _context6.next = 16;
                    return (0, _reduxSaga.takeLatest)(types.SHOW_NOTIFICATION_ERROR, hideNotification);

                case 16:
                case 'end':
                    return _context6.stop();
            }
        }
    }, _marked[5], this);
}

exports.default = sagas;