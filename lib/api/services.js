"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.get = get;
exports.post = post;
exports.getReviews = getReviews;
exports.createReview = createReview;
exports.sendEmail = sendEmail;
exports.getProjects = getProjects;

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// API - GET
function get(url) {
    try {
        return _axios2.default.get(process.env.API_URL + url);
    } catch (e) {
        throw e;
    }
}

// API - POST
function post(url, payload) {
    try {
        return _axios2.default.post(process.env.API_URL + url, payload);
    } catch (e) {
        throw e;
    }
}

function getReviews() {
    return get("/review/1/20");
}

function createReview(review) {
    return post("/review", review);
}

function sendEmail(email) {
    return post("/contact", email);
}

function getProjects() {
    return get("/project/1/20");
}